const link = 'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture',
 wrapper = document.querySelector('.personalItems'),
 modalWrapper = document.querySelector('.modal-hide'),
 modal = document.querySelector('.modal'),
 filterSelector = document.querySelector('#sortItems');

request(link)
 .then(response => {
   const profiles = JSON.parse(response);
   //console.log(profiles.results);
    filterItems(profiles.results);
    modalMetods(profiles.results);
    loadEventListeners(profiles.results);
 });


function request(url) {

  return new Promise(function(resolve, reject) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onload = function() {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        var error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function() {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });

}


function renderItems(results) {
  let template = '';
  results.forEach(function(element, id) {
        template += `
        <div class="personalItemWrap">
          <div class="personalItem">
            <div class="personalItem__img">
                <img src="${ element.picture.medium }">
            </div>
            <div class="personalItem__desc">
              ${element.name.title} ${ element.name.first } ${ element.name.last }
            </div>
            <a href="#modal" class="personalItem__link" id="${id}"></a>
          </div>  
        </div>
        `;
  });
  wrapper.innerHTML = template;
}

function filterItems(results){

  results.sort(function (namePrev, nameNext) {
    if(filterSelector.selectedIndex === 0){
      return namePrev.name.first.localeCompare(nameNext.name.first);
    }
    else if(filterSelector.selectedIndex === 1){
      return nameNext.name.first.localeCompare(namePrev.name.first);
    }
  });

  renderItems(results);
}


function loadEventListeners(profiles){

  filterSelector.addEventListener('change', () => {filterItems(profiles)});

};

function modalMetods(profiles){

  wrapper.addEventListener('click', (e) => {
    openModal(e, profiles);
  });

  modalWrapper.addEventListener('click', closeModal);

}


function openModal(e, profilesData) {
  const link = e.target;
  if(link.getAttribute('href') === '#modal'){
    fillModal(link, profilesData);
    modalWrapper.classList.add('in');
    e.preventDefault();
  }
};

function closeModal(e) {
  if(e.target.dataset.modal === 'close' || e.target === modalWrapper){
    modalWrapper.classList.remove('in');
    modal.innerHTML = ' ';
  }
};

function fillModal(item, jsn){
  const num = parseInt(item.id);
  let template = `
    <div class="modal__img"><img src="${jsn[num].picture.large}"></div>
    <div class="modal__close" data-modal="close"></div>
    <div class="modal__title">
      <h2>${jsn[num].name.title} ${ jsn[num].name.first } ${ jsn[num].name.last }</h2>
      <p>Gender: ${jsn[num].gender}</p>
    </div>
    <div class="modal__body">
      <div class="modal__body-section">
        <h3><b>Address:</b></h3>
      <p><b>Street:</b> ${checkVariable(jsn[num].location.street)}</p>
      <p><b>City:</b> ${checkVariable(jsn[num].location.city)}</p>
      <p><b>State:</b> ${checkVariable(jsn[num].location.state)}</p>
      </div>
      <div class="modal__body-section">
        <h3><b>Dates:</b></h3>
        <p><b>Email:</b> ${checkVariable(jsn[num].location.email)}</p>
        <p><b>Phone:</b> ${checkVariable(jsn[num].phone)}</p>
      </div>  
    </div>
  `;

  modal.innerHTML = template;
}


function checkVariable(variable){
  if(variable !== undefined){
    return variable;
  }
  else{
    return '-';
  }
}





